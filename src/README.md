## This is where your Express app goes

- Clone your project or copy all of the files directly into this `src` directory.
- Spin up the Docker network by following the instructions on the main [README.md](../README.md)