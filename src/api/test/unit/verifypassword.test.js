require( 'dotenv' ).config();
const userModel = require( '../../models/user' );
require('../../db/database')

describe( 'Test for veryfy password', function () {
    test( 'should return a true for correct password', async () => {

        let user = new userModel();
        user.username = "test@koibanx.com";
        user.password = "admin";
        const userCreated = await userModel.create(user);
        expect(userCreated).not.toBe(null);
        const result = userCreated.verifyPassword("admin")
        expect(result).toEqual(true)

    });
    test( 'should return a false for incorrect password', async () => {

        let user = new userModel();
        user.username = "test@koibanx.com";
        user.password = "admin";
        const userCreated = await userModel.create(user);
        expect(userCreated).not.toBe(null);
        const result = userCreated.verifyPassword("admin123")
        expect(result).toEqual(false)

    });
    afterEach( async ( ) => {
       
        await userModel.remove({});
    })
});