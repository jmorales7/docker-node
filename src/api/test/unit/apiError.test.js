const ApiError = require("../../error/api.error");

describe( 'Test Api Error class and methods', function () {
    test( 'return corret properties ', async () => {
        const response = new ApiError(400,'Bad Request');
        expect(response).toHaveProperty('code')
        expect(response).toHaveProperty('message')
    });
    test( 'return corret properties for method badrequest ', async () => {
        const response = ApiError.badRequest('Bad Request');
        expect(response).toHaveProperty('code');
        expect(response).toHaveProperty('message');
        expect(response.code).toBe(400);
    });
    test( 'return corret properties for method internal ', async () => {
        const response = ApiError.internal('Internal Error');
        expect(response).toHaveProperty('code');
        expect(response).toHaveProperty('message');
        expect(response.code).toBe(500);
    });
});