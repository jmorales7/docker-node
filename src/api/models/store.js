const mongoose = require('mongoose');

const StoreSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true,'The field is required']
  },
  cuit: {
    type: String,
    required: [true,'The field is required']
  },
  concepts: {
    type: Array,
    required: [true,'The field is required']
  },
  currentBalance: {
    type: Number,
    required: [true,'The field is required']
  },
  active: {
    type: Boolean,
    required: [true,'The field is required']
    
  },
  lastSale: {
    type: Date,
    required: [true,'The field is required'],
    validate: {
      validator: function(v) {

        return v instanceof Date;
        
      },
      message: props => `${props.value} is not a date!`
    }
  }
},{ timestamps: true });

StoreSchema.pre('save', async function (callback) {
  //completar de ser necesario
});

module.exports = mongoose.model('Store', StoreSchema);
