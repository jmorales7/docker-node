# Node js / Express Js
This application has two endpoints built are the following:

- **api/stores  GET** - `:returns all the stores stored with pagination and receives the parameters page, limit, and q={} (where q are the statements that mongoose uses)`
- **api/stores POST** - `:receives the parameters to create a store and stores it`
- parameters to store:
- name: string
- cuit: string
- concepts: array
- currentBalance: number 
- active: boolean 
- lastSale: date 

