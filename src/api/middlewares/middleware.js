const userModel = require('../models/user');

module.exports = function  (req,res,next) {

    const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
    

    const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')
    

    userModel.findOne({ 'username': login }, 'password', function (err, user) {
        
        if(user && user.verifyPassword(password)) {
            next();
        } else {
            return res.status(401).send({
                success:false,
                message: 'Usuario y/o contraseña incorrectos',
                data: null
            })
        }
    });

}