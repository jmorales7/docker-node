const ApiError = require('./api.error');
const logger = require('../utils/logger');

function apiErrorHandler(err, req, res, next) {
 
  logger.error(err);

  if (err instanceof ApiError) {
    res.status(err.code).send({'message':err.message});
    return;
  }

  res.status(500).send({'message':'Something went wrong'});
}

module.exports = apiErrorHandler;