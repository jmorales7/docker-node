const router = require( 'express' ).Router();
const storeRoutes = require('./stores');
const authMiddleware = require('../middlewares/middleware');
const apiErrorHandler = require('../error/api.error.handler');


router.use('/stores',authMiddleware,storeRoutes);
router.use(apiErrorHandler)

module.exports = router;