const storeModel = require('../models/store')

module.exports = {
    getData: async data => {
        const {limit=10, page=1, q={}} =data; 

        let search = {}

        if( JSON.stringify(q) !== '{}') {
            let query = q.replace(/(\w+:)|(\w+ :)/g, function(matchedStr) {
                return '"' + matchedStr.substring(0, matchedStr.length - 1) + '":';
                });
            search = JSON.parse(query);
        }
       
       
        const stores = await storeModel.aggregate([
            {
                $facet: {
                    data: [
                        { $match: search },
                        { $project: {_id:0,name: 1, cuit: 1, concepts: 1, currentBalance: 1,active:1,lastSale:1}},
                        { $skip: parseInt((page - 1) * limit) },
                        { $limit: parseInt(limit) },
                    ]
                }
            }
        ])

        const total = await storeModel.find(search).countDocuments()             

        const totalPages =  Math.ceil(total / limit)

       

        return {
            'data': stores[0].data,
            'page': page,
            'pages': totalPages,
            'limit': parseInt(limit),
            'total': total
        }
    },
    saveData: async  data => {

        const {name,cuit,concepts,currentBalance,active,lastSale} = data;
        

        const store = await new storeModel ();
        store.name = name;
        store.cuit =  cuit;
        store.concepts= concepts;
        store.currentBalance= currentBalance;
        store.active= active;
        store.lastSale= lastSale;
        
        await store.save()
       
    }
}