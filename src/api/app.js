const express = require('express')

const app = express()

var bodyParser = require('body-parser')

const dotenv = require('dotenv');

dotenv.config();

require('./utils/initializer').init()

app.use(bodyParser.json())

app.use('/api', require('./routes/index'));



module.exports = app
