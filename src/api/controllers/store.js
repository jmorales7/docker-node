const storeService = require('../services/store.service');
const apiError = require('../error/api.error');

module.exports = {
    index: async (req,res,next) => {
        try {

            const responseServiceStore = await storeService.getData(req.query);

            return res.status(200).send(responseServiceStore)

        } catch (error) {
             next(error);
        }
       
    },
    store: async (req,res,next) => {
        try {
            
            await storeService.saveData(req.body);
            
            res.status(201).send({'message': "Store craeted!"});
           
        } catch (error) {
            
            if(error.name == 'ValidationError') next(apiError.badRequest(error.message));

            next(error);
           
            
        }
        
    }
}