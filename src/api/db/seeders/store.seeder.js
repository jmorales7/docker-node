const dotenv = require('dotenv');
dotenv.config();
const config = require('config');
const mongoose = require('mongoose')
const storeModel = require('../../models/store');
const logger = require('../../utils/logger');


mongoose.connect('mongodb://' + config.get('mongodb.address') + '/' + config.get('mongodb.dbname'), { useNewUrlParser: true, useUnifiedTopology: true }).then(()=> {
    logger.info('DB connected')
}).catch(error => {
    logger.error(error)
})

const seedStores = [];
for(var i = 0; i<=100; i++) {
    
        seedStores.push({
            name: "Esta es "+i,
            cuit:"Este es mi cuit"+1,
            concepts: 
            [
                "prueba_1-"+i,
                "prueba_2-"+i
            ],
            currentBalance: 30,
            active: true,
            lastSale:"2022-07-25"
        })
}

const seedDB = async () => {
    await storeModel.deleteMany({});
    await storeModel.insertMany(seedStores)
}

seedDB().then(()=> {
    logger.info('ready')
    mongoose.connection.close()
    return;
})