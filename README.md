# docker-compose-node
A pretty simplified Docker Compose workflow that sets up a network of containers for local Express js development.


## Usage

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this, and spin up the containers for the web server by running `docker-compose up -d --build`.

After that completes, follow the steps from the [src/README.md](src/README.md) file to get your Express js project added in.

 The following containers are built for our web server, with their exposed ports detailed:

- **node** - `:3001`
- **mongo** - `:27018`

One additional containers are included that handle NPM commands *without* having to have these platforms installed on your local computer. Use the following command from your project root.

- `docker exec -ti npm sh`: This command will help us to enter the terminal of our container, where we can use npm to run the following commands: *npm install* to install all dependencies, *npm test* to be able to run the tests and *npm run seed* to migrate data to the database

## Persistent Mongo Storage

you  have persistent data that remains after bringing containers down