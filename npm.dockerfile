FROM node:17.8.0
WORKDIR /usr/src/app

CMD ["npm", "run", "start"]